# Soal-Shift-Sisop-Modul-3-A13-2022

## daftar isi ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomer 1](#nomer-1)
    - [Soal  1.A](#soal-1a)
    - [Soal  1.B](#soal-1b)
    - [Soal  1.C](#soal-1c)
    - [Soal  1.D](#soal-1d)
- [Nomer 2](#nomer-2)
    - [Soal  2.A](#soal-2a)
    - [Soal  2.B](#soal-2b)
    - [Soal  2.C](#soal-2c)
    - [Soal  2.D](#soal-2d)
    - [Soal  2.E](#soal-2e)
- [Nomer 2](#nomer-3)
    - [Soal  3.A](#soal-3a)
    - [Soal  3.B](#soal-3b)
    - [Soal  3.C](#soal-3c)
    - [Soal  3.D](#soal-3d)

## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201004  | Danial Farros Maulana | SISOP C
5025201038    | Cholid Junoto | SISOP A
5025201220    | Davian Benito | SISOP A

## Nomer 1 ##

Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

### Soal 1a ###
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

**Penyelesaian Soal**

langkah awal penyelesaian soal ini dilakukan dengan mendownload file yang ada pada google drive dengan perintah sebagai berikut
```c
char *download[][8] = {
	{ "wget", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "music.zip", NULL},
	{ "wget", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "quotes.zip", NULL}
};

for (int i=0;i<2;i++){
	if (fork()==0) continue;
	execv ("/usr/bin/wget",download[i]);
}
```
langkah selanjutnya adalah dilakukan dengan mengunzip kedua file, akan tetpi pada proses unzip dilakukan dengan menggunakan thread

```c
pid_t child_id;
child_id = fork();
int status;
if (child_id < 0) {
	exit(EXIT_FAILURE);
}

if (child_id == 0) {	
	sleep(10);
	int err;
	unsigned long i =0;
	while(i<2) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]),NULL,&playandcount,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
} 
else {
	while ((wait(&status)) > 0);
	zipping();
}
```
pada fungsi di atas juga dilakukan pemanggilan fungsi pendamping yang merupakan bagian dari thread
```c
void* playandcount(void *arg) {
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid[0])) //thread untuk clear layar
	{
		int status;		
		child = fork();
		if (child==0) {
			char *ekstrak[5] = {"unzip", "music.zip", NULL};
			execv ("/usr/bin/unzip",ekstrak);
	    }
		else {
			//fungsi pendukung untuk melakukan decode
			}
		}
	}
	else if(pthread_equal(id,tid[1])) // thread menampilkan counter
	{	
		int status;
		child = fork();
		if (child==0) {
			char *ekstrak[5] = {"unzip", "quotes.zip", NULL};
			execv ("/usr/bin/unzip",ekstrak);
	    }
		else {
			//fungsi pendukung untuk melakukan decode
		}
	}
	return NULL;
}
```

### Soal 1b ###

Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

** Penyelesaian Soal **

proses decodeing dilakukan dengan menggunakna fungsi berikut
```c
char* base64Decoder(char encoded[], int len_str)
{
	char* decoded_string;
	decoded_string = (char*)malloc(sizeof(char) * SIZE);
	int i, j, k = 0;
	int num = 0;
	int count_bits = 0;
	for (i = 0; i < len_str; i += 4) {
		num = 0, count_bits = 0;
		for (j = 0; j < 4; j++) {
			if (encoded[i + j] != '=') {
				num = num << 6;
				count_bits += 6;
			}
			if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
				num = num | (encoded[i + j] - 'A');

			else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
				num = num | (encoded[i + j] - 'a' + 26);
			else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
				num = num | (encoded[i + j] - '0' + 52);
			else if (encoded[i + j] == '+')
				num = num | 62;
			else if (encoded[i + j] == '/')
				num = num | 63;
			else {
				num = num >> 2;
				count_bits -= 2;
			}
		}
		while (count_bits != 0) {
			count_bits -= 8;
			decoded_string[k++] = (num >> count_bits) & 255;
		}
	}
	decoded_string[k] = '\0';
	return decoded_string;
}
```
pemanggilan fungsi di atas akan mengembalikan karakter hasil decode, proses dekoding isi file dilakukan dengan menstore nama file uyang ada pada folder music dan juga quotes menuju array global untuk dilakukan proses decoding setiap file
```c
//fungsi untuk megembalikan nama file
void getmusic(){
	DIR *d;
	struct dirent *dir;
	d = opendir(".");
	if (d){
	int count =0;
	while ((dir = readdir(d)) != NULL){
		if (strlen(dir->d_name) > 5 && dir->d_name[0] == 'm' && dir->d_name[2] == '.'){
			strcpy(music[count], dir->d_name);
			printf(">>%s\n",music[count]);
			count++;
			}
		}
	closedir(d);
	musiccount = count;
	}
}

void getquotes(){
	DIR *d;
	struct dirent *dir;
	d = opendir(".");
	if (d){
	int count =0;
	while ((dir = readdir(d)) != NULL){
		if (strlen(dir->d_name) > 5 && dir->d_name[0] == 'q' && dir->d_name[2] == '.'){
			strcpy(music[count], dir->d_name);
			printf(">>%s\n",music[count]);
			count++;
			}
		}
	closedir(d);
	quotescount = count;
	}	
}
```
setelah didapatan nama nama file yang ada didalam folder akan dilakukan proses decoding sebagai bebrikut, dengan proses pemanggilan dilakukan didalam thread
```c
//proses decoding untuk file music
getmusic();
char *filename = "music.txt";
FILE *fw = fopen(filename, "w");
if (fw == NULL)
{
	printf("Error opening the file %s", filename);
	return 0;
}

for (int i = 0; i < musiccount; i++){
	printf("%d.store\n",i);
	FILE *fptr;
	fptr = fopen(music[i], "r");

	char encoded_string[100];
	printf("%s\n",encoded_string);
	
	fscanf(fptr,"%s", encoded_string);
	int len_str = strlen(encoded_string);
	printf("%s\n",base64Decoder(encoded_string, len_str));
	fprintf(fw, "%s\n", base64Decoder(encoded_string, len_str));
}

fclose(fw);
```
```c
getquotes();
// proses decoding untuk file quotes
char *filename = "quotes.txt";
FILE *fw = fopen(filename, "w");
if (fw == NULL)
{
	printf("Error opening the file %s", filename);
	return 0;
}

for (int i = 0; i < quotescount; i++){
	printf("%d.store\n",i);
	FILE *fptr;
	fptr = fopen(music[i], "r");

	char encoded_string[100];
	printf("%s\n",encoded_string);
	
	fscanf(fptr,"%s", encoded_string);
	int len_str = strlen(encoded_string);
	printf("%s\n",base64Decoder(encoded_string, len_str));
	fprintf(fw, "%s\n", base64Decoder(encoded_string, len_str));
}

fclose(fw);
```

### Soal 1c ###
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
** Penyelesaian Soal **
proses pemindahan dilakuan dengan menggunakan fungsi `filemove` 
```c
//moving file untuk file quotes
	filemove("quotes.txt","./hasil");
//moving file untuk file music
	filemove("quotes.txt","./hasil");
```

### Soal 1d ###
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
** Penyelesaian Soal **
proses zipping dilakukan dengan menggunakan kode sebagai berikut
```c
	char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenestdanial", "hasil", NULL}; 
	execv("/usr/bin/zip", args);
```

### Soal 1c ###
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
** Penyelesaian Soal **
proses pembuatan thread dilakukan dengan program sebagai berikut
```c
sleep(10);
int err;
unsigned long i =0;
while(i<2) // loop sejumlah thread
{
	err=pthread_create(&(tid1[i]),NULL,&playandcount1,NULL); //membuat thread
	if(err!=0) //cek error
	{
		printf("\n can't create thread : [%s]",strerror(err));
	}
	else
	{
		printf("\n create thread success\n");
	}
	i++;
}
pthread_join(tid1[0],NULL);
pthread_join(tid1[1],NULL);
} 
```
pada thread ini juga dilakukan pemanggilan fungsi pendukung thread yakni
```c
pid_t child1;
pthread_t tid1[2]; 
void* playandcount1(void *arg) {
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid1[0])) //thread untuk clear layar
	{
		pid_t child_id;
  		child_id = fork();
  		if(child_id != 0){
    		char *ekstrak[5] = {"unzip", "-P","mihinomenestdanial", "hasil.zip",  NULL};
			execv ("/usr/bin/unzip",ekstrak);
  		}
		
		else {
    		printf("tutup\n");
			return;
  		}

	}
	else if(pthread_equal(id,tid1[1])) // thread menampilkan counter
	{	
		char *filename = "no.txt";
		FILE *fw = fopen(filename, "w");
		if (fw == NULL)
		{
			printf("Error opening the file %s", filename);
			return 0;
		}
		fprintf(fw, "NO\n");
		fclose(fw);
	}
	return NULL;
}
```
setelah dilakukan pemanggilan thread diatas akan dilakukan pengezipan ulang dengan menunggu proses telah sepenuhnya berjalan sempurna 
```c
while ((wait(&status)) > 0);
		char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenestdanial", "hasil", "no.txt", NULL};
        execv("/usr/bin/zip", args);
```
## Nomer 2 ##
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

### Soal 2a ###
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
Username unique (tidak boleh ada user yang memiliki username yang sama)
Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
Format users.txt:
users.txt
username:password
username2:password2

Pertama kita buat 2 script, server.c dan client.c dan kita hubungkan menggunakan socket. Kemudian untuk bagian sisi client, client hanya perlu dapat mengirimkan pesan dan mendengarkan respon jawaban dari server. 
``` c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }


    char input[100] = "";
    while(1){
    	fgets(input, 100, stdin);
    	send(sock , input , strlen(input) , 0 );
    	printf("message sent\n");
   		valread = read( sock , buffer, 1024);
    	printf("%s\n\n",buffer );
    }
    
    return 0;
}

```

Sedangkan untuk bagian sisi server, server harus dapat mengimplementasikan ketentuan-ketentuan yang diminta soal, untuk yang ini, server menyediakan register dan login. Pertama kita bandingkan apakah input client merupakan "register" atau "login", bila iya, maka masuk ke logic didalam if statement tersebut. 
1. Pada register, inputan username kita cek apakah tersedia atau tidak dengan fungsi "checkStringFile()" yang berfungsi membuka user.txt dan melakukan pengecekan, lalu untuk password, dapat kita cek dengan fungsi "checkPasswordValid()" yang berfungsi melakukan penyaringan if statement sesuai ketentuan password di soal. Apabila telah lulus keduanya, maka dipanggil fungsi writeFilezPassword(inputz) berfungsi untuk menuliskan register user ke dalam data.txt
2. Pada login, inputan username dan password kita cek dengan fungsi "checkStringFile()" dan "checkPasswordFile" yang berfungsi untuk membuka user.txt dan melakukan pengecekan terhadap username dan password tersebut. Bila lulus, maka user dapat mulai memanggil fungsi2 lainnya
```c
if(strcmp(buffer, "register\n") == 0 && !logedIn){
	//FOR USERNAME INPUT
  	send(new_socket , "Enter your username : " , strlen("Enter your username : ") , 0 );
   	valread = read( new_socket , buffer, 1024);
   	printf("username: %s", buffer);
   	strcpy(inputz, buffer);
   	endLineErase(inputz);
   			
   	usernameExist = checkStringFile(inputz);
   	while(usernameExist){
   		send(new_socket , "Username alrady taken" , strlen("Username alrady taken") , 0 );
   		valread = read( new_socket , buffer, 1024);
   		strcpy(inputz, buffer);
   		endLineErase(inputz);
   		usernameExist = checkStringFile(inputz);
   	}
   	writeFilezUsername(inputz);
   	
   	//FOR PASSWORD INPUT
   	send(new_socket , "Enter your password : " , strlen("Enter your password : ") , 0 );
   	valread = read( new_socket , buffer, 1024);
   	strcpy(inputz, buffer);
   	endLineErase(inputz);
   	
   	passwordValid = checkPasswordValid(inputz);
   	while(!passwordValid){
   		send(new_socket , "Password must contain 6 letter, number, capital and lowercase letter" , strlen("Password must contain 6 letter, number, capital and lowercase letter") , 0 );
   		valread = read( new_socket , buffer, 1024);
   		strcpy(inputz, buffer);
   		endLineErase(inputz);
   		passwordValid = checkPasswordValid(inputz);
   	}
   	writeFilezPassword(inputz);
   	
   	send(new_socket , "Successfully Registered\nCommand: ..." , strlen("Successfully Registered\nCommand: ...") , 0 );
}
else if (strcmp(buffer, "login\n") == 0 && !logedIn){
   	//
   	//
    //FOR LOGIN USERNAME INPUT
   	send(new_socket , "Enter your login username : " , strlen("Enter your login username : ") , 0 );
   	valread = read( new_socket , buffer, 1024);
   	strcpy(inputz, buffer);
   	endLineErase(inputz);
   			
   	usernameExist = checkStringFile(inputz);
   	while(!usernameExist){
   		send(new_socket , "Username not exist" , strlen("Username not exist") , 0 );
   		valread = read( new_socket , buffer, 1024);
   		strcpy(inputz, buffer);
   		endLineErase(inputz);
   		usernameExist = checkStringFile(inputz);
   	}

   	strcpy(username, inputz);
   	
   	//FOR LOGIN PASSWORD INPUT
   	send(new_socket , "Enter your login password : " , strlen("Enter your login password : ") , 0 );
   	valread = read( new_socket , buffer, 1024);
   	strcpy(inputz, buffer);
   	endLineErase(inputz);
   		
   	passwordValid = checkPasswordFile(inputz, username);
   	while(!passwordValid){
   		send(new_socket , "Password not match" , strlen("Password not match") , 0 );
   		valread = read( new_socket , buffer, 1024);
   		strcpy(inputz, buffer);
   		endLineErase(inputz);
   		passwordValid = checkPasswordFile(inputz, username);
   	}
   	logedIn = true;
   	send(new_socket , "Successfully Login\nCommand: 1. add    2. see    3. download <judul-problem>    4. submit <judul-problem> <path-file-output.txt>" , strlen("Successfully Login\nCommand: 1. add    2. see    3. download <judul-problem>    4. submit <judul-problem> <path-file-output.txt>") , 0 );
}
```

### Soal 2b ###
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

Inisialisasi pembuatan file ini dapat kita lakukan dengan memakai fopen dan fprintf file.tsv yang diminta yang kemudian akan kita panggil di atas while loops utama program
```c
FILE * fPtr2;
fPtr2 = fopen("problems.tsv", "a+");
fprintf(fPtr2, "judul\tauthor\n");
fclose(fPtr2);
```

### Soal 2c ###
Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv.


Pertama dari server meminta inputan data-data yang diminta ke client lalu memasukkannya ke variablenya. Lalu dari data yang didapatkan tersebut, akan dicopykan ke folder <judul> (folder ini dibuat memakai mkdir() di sisi server) pada sisi server memakai fungsi copyFile(source, destination) yang berfungsi untuk mengcopykan file dari source ke destination. Pada penambahan ini, tidak lupa kita tambahkan data pada file database problems.tsv kita memakai fungsi writeDatabase(judul, username) berfungsi untuk melakukan appends data ke file problems.tsv
```c
		else if(logedIn && strcmp(buffer, "add\n") == 0){
   			char judul[100] = "";
   			char descriptionPath[100] = "";
   			char inputPath[100] = "";
   			char outputPath[100] = "";
   			send(new_socket , "Judul problem:" , strlen("Judul problem:") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(judul, buffer);
   			endLineErase(judul);
   			
   			send(new_socket , "Filepath description.txt:" , strlen("Filepath description.txt:") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(descriptionPath, buffer);
   			endLineErase(descriptionPath);
   			
   			send(new_socket , "Filepath input.txt:" , strlen("Filepath input.txt:") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(inputPath, buffer);
   			endLineErase(inputPath);
   			
   			send(new_socket , "Filepath output.txt:" , strlen("Filepath output.txt:") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(outputPath, buffer);
   			endLineErase(outputPath);
   			
   			writeDatabase(judul, username);
   			int check = mkdir(judul, 0777);
   			char pathJudul[100] = "";
   			
   			//copy descriptionfile
   			char pathDescriptionClient[100] = "";
   			strcpy(pathDescriptionClient, "../client/");
   			strcat(pathDescriptionClient, descriptionPath);
   			
   			strcpy(pathJudul, judul);
   			strcat(pathJudul, "/descriptions.txt");
   			copyFile(pathDescriptionClient, pathJudul);
   			
   			//copy inputFile
   			char pathInputClient[100] = "";
   			strcpy(pathInputClient, "../client/");
   			strcat(pathInputClient, inputPath);
   			
   			strcpy(pathJudul, judul);
   			strcat(pathJudul, "/inputs.txt");
   			copyFile(pathInputClient, pathJudul);
   			
   			//copy outputFile
   			char pathOutputClient[100] = "";
   			strcpy(pathOutputClient, "../client/");
   			strcat(pathOutputClient, outputPath);
   			
   			strcpy(pathJudul, judul);
   			strcat(pathJudul, "/outputs.txt");
   			copyFile(pathOutputClient, pathJudul);
   			
   			send(new_socket , "Add complete" , strlen("Add complete") , 0 );
   			
   		}
```

### Soal 2d ###
Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:
<judul-problem-1> by <author1>
<judul-problem-2> by <author2>

Ini dapat kita lakukan dengan memakai fungsi seeProblem() yang berfungsi untuk membuka file database dan melakukan ekstrak nama judul dan authornya, lalu menampilkannya di server

```c
void seeProblem(){
	ssize_t read;
	char* ret;
	char* retP;
	char* line;
	size_t len = 0;

	FILE *fp = fopen("problems.tsv", "r");
	if(!fp){
		printf("Failed to open file\n");
		return;
	}
	
	while(fscanf(fp, "%s\n", line) != EOF){
		bool problemz = true;
		char problem[100] = "";
		char author[100] = "";
		int j = 0;
		line[strlen(line) - 1] = '\0';
		for(int i = 0; i < strlen(line); i++){
			if(line[i] == '	'){
				problem[i] = '\0';
				problemz = false;
				continue;	
			}
			if(line[i] == '\n');{
				author[j] = '\0';
				break;
			}
			if(problemz){
				problem[i] = line[i];
			}
			else if(!problemz){
				author[j] = line[i];
				j++;
			}
		}
		printf("%s by %s\n", problem, author);
	}
	fclose(fp);
	return;
}
```
### Soal 2e ###
Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

Dapat kita lakukan dengan memakai fungsi copyFile(source, destination) sebelumnya, namun bedanya untuk ini, source berada di sisi server dan destination berada di sisi client dengan foldernya yaitu <problem> yang dapat kita buat memakai mkdir() seperti sebelumnya
```c
else if(logedIn && strstr(buffer, "download")){
   			char judul[100] = "";
   			judulDownload(buffer, judul);
   			char pathClient[100] = "";
   			char clientDescription[100] = "";
   			char clientInput[100] = "";
   			
   			//make directory in client
   			strcpy(pathClient, "../client/");
   			strcat(pathClient, judul);
   			strcat(pathClient, "/");
   			int checkz = mkdir(pathClient, 0777);
   			
   			//path in client
   			strcpy(clientDescription, pathClient);
   			strcat(clientDescription, "descriptions.txt");
   			strcpy(clientInput, pathClient);
   			strcat(clientInput, "inputs.txt");
   			
   			//path in server
   			char pathDescription[100] = "";
   			char pathInput[100] = "";
   			strcpy(pathDescription, judul);
   			strcat(pathDescription, "/description.txt");
   			strcpy(pathInput, judul);
   			strcat(pathInput, "/input.txt");
   			
   			//copy files
   			copyFile(pathDescription, clientDescription);
   			copyFile(pathInput, clientInput);
   			
   			send(new_socket , "Download complete" , strlen("Download complete") , 0 );
   		}
```

### Soal 2f ###
Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

Di sini dapat kita lakukan dengan memakai fungsi eval(outputPath, outputClientPath) yang berfungsi untuk melakukan perbandingan isi dari file yang berada di kedua outputPath tersebut, bila sama maka akan mengembalikan nilai true dan bila ada yang berbeda maka akan mengembalikan false. Selanjutnya berdasarkan nilai boolean tersebut, server akan menampilkan pesan "AC" / "WA" sesuai ketentuan soal
```c
bool eval(char* outputPath, char* outputClientPath){
	FILE * fPtr1;
	FILE * fPtr2;
	fPtr1 = fopen(outputPath, "r");
	fPtr2 = fopen(outputClientPath, "r");
	
	if(fPtr1 == NULL || fPtr2 == NULL){
		return false;
	}
	
	char ch1, ch2;
	do{
		ch1 = fgetc(fPtr1);
		ch2 = fgetc(fPtr2);
		
		if(ch1 != ch2){
			fclose(fPtr1);
			fclose(fPtr2);
			return false;
		}
	}while(ch1 != EOF && ch2 != EOF);
	
	fclose(fPtr1);
	fclose(fPtr2);
	return true;
}
```

### Soal 2g ###
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

Model dari program socket ini telah dapat menangani multiple client connection pada sisi servernya

## Nomer 3 ##
