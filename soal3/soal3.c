#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>

//fungsi buat cek eksis file
int cfileexists(const char *filename)
{
    struct stat buffer;
    int exist = stat(filename, &buffer);
    if (exist == 0)
        return 1;
    else // -1
        return 0;
}

//fungsi move
void *move(void *filename)
{
    char cwd[PATH_MAX];
    char dirname[200], hidden[100], hiddenname[100], file[100], existsfile[100];
    int i;
    strcpy(existsfile, filename);
    strcpy(hiddenname, filename);
    char *namaa = strrchr(hiddenname, '/');
    strcpy(hidden, namaa);

    //Untuk file yang hidden, awalan .
    if (hidden[1] == '.')
    {
        strcpy(dirname, "Hidden");
    }
    //File biasa
    else if (strstr(filename, ".") != NULL)
    {
        strcpy(file, filename);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        //nggak case sensitive
        for (i = 0; token[i]; i++)
        {
            token[i] = tolower(token[i]);
        }
        strcpy(dirname, token);
    }
    //file gaada extensi
    else
    {
        strcpy(dirname, "Unknown");
    }
	char newloct [100]; 
	strcpy(newloct, "/home/danialfarros/shift3/hasil/");
    //cek file ada ato ga, kalo ga dibuat folder
    int exist = cfileexists(existsfile);
	printf("exists file --> %s \n",existsfile);
    if (exist){
		strcat(newloct, dirname);
        mkdir(dirname, 0755);
		mkdir(newloct, 0755);
	}
    //dapet nama file
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama = strrchr(filename, '/');
        char namafile[200];
        //strcpy(namafile, cwd);
        strcpy(namafile, "/home/danialfarros/shift3/hasil/");
        strcat(namafile, dirname);
        strcat(namafile, nama);
		printf("namafile --> %s \n",namafile);

        //move file pake rename
        rename(filename, namafile);
    }
}

//rekursif list
void listFilesRecursively(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(basePath);
    int n = 0;

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            // Construct new path from our base path
            strcpy(path, basePath);	
            strcat(path, "/");
            strcat(path, dp->d_name);
			printf("%s\n", path);

            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {
                //membuat thread untuk cek move
                pthread_t thread;
                int err = pthread_create(&thread, NULL, move, (void *)path);
                pthread_join(thread, NULL);
            }

            listFilesRecursively(path);
        }
    }
    closedir(dir);
}

//fungsi main
int main(int argc, char *argv[])
{
    listFilesRecursively("hartakarun");
	mkdir("/home/danialfarros/shift3/", 0755);
	mkdir("/home/danialfarros/shift3/hasil/", 0755);
    struct stat buffer;
    int err = stat("hartakarun", &buffer);
    if (err == -1)
    {
        printf("Yah, gagal disimpan :(\n");
    }
    else
    {
        printf("Direktori sukses disimpan!\n");
    }
}
