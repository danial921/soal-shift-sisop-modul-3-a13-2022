#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <regex.h>
#include <sys/stat.h>
#define PORT 8080

void endLineErase(char* input){
	
	for(int i = 0; i < strlen(input); i++){
		if(input[i] == '\n'){
			input[i] = '\0';
			return;
		}
	}
}

bool checkStringFile(char* input){
	ssize_t read;
	char* ret;
	char* line;
	size_t len = 0;

	FILE *fp = fopen("users.txt", "r");
	if(!fp){
		printf("Failed to open file\n");
		return false;
	}
	
	while(fscanf(fp, "%s\n", line) != EOF){
		line[strlen(line) - 1] = '\0';
		ret = strstr(line, input);
		if(ret){
			fclose(fp);
			if(line)
				free(line);
			return true;
		}
		else{
			fclose(fp);
			if(line)
				free(line);
			return false;
		}
	}
}

bool checkPasswordFile(char* password, char* username){
	ssize_t read;
	char* ret;
	char* retP;
	char* line;
	size_t len = 0;

	FILE *fp = fopen("users.txt", "r");
	if(!fp){
		printf("Failed to open file\n");
		return false;
	}
	
	while(fscanf(fp, "%s\n", line) != EOF){
		line[strlen(line) - 1] = '\0';
		ret = strstr(line, username);
		if(ret){
			retP = strstr(line, password);
			if(retP){
				fclose(fp);
				return true;
			}
			else{
				fclose(fp);
				return false;
			}
		}
		else{
			fclose(fp);
			return false;
		}
	}
}

void writeFilezUsername(char* input){
	char data[100] = "";
	strcpy(data, input);
	strcat(data, ":");
	FILE * fPtr;
    fPtr = fopen("users.txt", "a");
    fputs(data, fPtr);
    fclose(fPtr);
}

void writeFilezPassword(char* input){
	char data[100] = "";
	strcpy(data, input);
	strcat(data, "\n");
	FILE * fPtr;
    fPtr = fopen("users.txt", "a");
    fputs(data, fPtr);
    fclose(fPtr);
}

void writeDatabase(char* judul, char* author){
	FILE * fPtr;
	fPtr = fopen("problems.tsv", "a");
	fprintf(fPtr, "%s\t%s\n", judul, author);
	fclose(fPtr);
}


bool checkPasswordValid(char* input){
	bool lower = false;
	bool upper = false;
	bool number = false;
	for(int i = 0; i < strlen(input); i++){
		if(65 <= (int)input[i] <= 90){
			upper = true;
		}
		if(97 <= (int)input[i] <= 122){
			lower = true;
		}
		if(48 <= (int)input[i] <= 57){
			number = true;
		}
	}
	if(upper && lower && number){
		return true;
	}
	return false;
}

void seeProblem(){
	ssize_t read;
	char* ret;
	char* retP;
	char* line;
	size_t len = 0;

	FILE *fp = fopen("problems.tsv", "r");
	if(!fp){
		printf("Failed to open file\n");
		return;
	}
	
	while(fscanf(fp, "%s\n", line) != EOF){
		bool problemz = true;
		char problem[100] = "";
		char author[100] = "";
		int j = 0;
		line[strlen(line) - 1] = '\0';
		for(int i = 0; i < strlen(line); i++){
			if(line[i] == '	'){
				problem[i] = '\0';
				problemz = false;
				continue;	
			}
			if(line[i] == '\n');{
				author[j] = '\0';
				break;
			}
			if(problemz){
				problem[i] = line[i];
			}
			else if(!problemz){
				author[j] = line[i];
				j++;
			}
		}
		printf("%s by %s\n", problem, author);
	}
	fclose(fp);
	return;
}

void judulDownload(char* input, char* judul){
	bool judulz = false;
	int j = 0;
	for(int i = 0; i < strlen(input); i++){
		if(input[i] == ' '){
			judulz = true;
			continue;
		}
		if(input[i] == '\n'){
			judul[j] = '\0';
			return;
		}
		if(judulz){
			judul[j] = input[i];
			j++;
		}
	}
	judul[j] = '\0';
	return;
}

void judulPathSubmit(char* input, char* judul, char* output){
	bool judulz = false;
	bool outputz = false;
	int j = 0;
	int k = 0;
	for(int i = 0; i < strlen(input); i++){
		if(input[i] == ' ' && !judulz){
			judulz = true;
			continue;
		}
		if(input[i] == ' ' && judulz){
			judul[j] = '\0';
			judulz = false;
			outputz = true;
			continue;
		}
		if(input[i] == '\n'){
			output[k] = '\0';
			return;
		}
		if(judulz){
			judul[j] = input[i];
			j++;
		}
		if(outputz){
			output[k] = input[i];
			k++;
		}
	}
	output[k] = '\0';
	return;
}


bool eval(char* outputPath, char* outputClientPath){
	FILE * fPtr1;
	FILE * fPtr2;
	fPtr1 = fopen(outputPath, "r");
	fPtr2 = fopen(outputClientPath, "r");
	
	if(fPtr1 == NULL || fPtr2 == NULL){
		return false;
	}
	
	char ch1, ch2;
	do{
		ch1 = fgetc(fPtr1);
		ch2 = fgetc(fPtr2);
		
		if(ch1 != ch2){
			fclose(fPtr1);
			fclose(fPtr2);
			return false;
		}
	}while(ch1 != EOF && ch2 != EOF);
	
	fclose(fPtr1);
	fclose(fPtr2);
	return true;
}

void copyFile(char* source, char* destination){
	FILE * fPtr1;
	FILE * fPtr2;
	char* line;
	
	fPtr1 = fopen(source, "r");
	fPtr2 = fopen(destination, "a");
	
	while(fscanf(fPtr1, "%s\n", line) != EOF){
		fputs(line, fPtr2);
	}
	
	fclose(fPtr1);
	fclose(fPtr2);
	return;
}


int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
    bool usernameExist = true;
    bool passwordValid = false;
    char username[100] = "";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    
    FILE * fPtr;
    fPtr = fopen("users.txt", "a+");
    fclose(fPtr);
    
    FILE * fPtr2;
    fPtr2 = fopen("problems.tsv", "a+");
    fprintf(fPtr2, "judul\tauthor\n");
    fclose(fPtr2);
    
    bool logedIn = false;
    
    char inputz[100] = "";
    
    while(valread = read( new_socket , buffer, 1024) > 0){
    	//valread = read( new_socket , buffer, 1024);
   		printf("%s\n\n",buffer );
   		if(strcmp(buffer, "register\n") == 0 && !logedIn){
   			//FOR USERNAME INPUT
   			send(new_socket , "Enter your username : " , strlen("Enter your username : ") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			printf("username: %s", buffer);
   			strcpy(inputz, buffer);
   			endLineErase(inputz);
   			
   			usernameExist = checkStringFile(inputz);
   			while(usernameExist){
   				send(new_socket , "Username alrady taken" , strlen("Username alrady taken") , 0 );
   				valread = read( new_socket , buffer, 1024);
   				strcpy(inputz, buffer);
   				endLineErase(inputz);
   				usernameExist = checkStringFile(inputz);
   			}
   			writeFilezUsername(inputz);
   			
   			//FOR PASSWORD INPUT
   			send(new_socket , "Enter your password : " , strlen("Enter your password : ") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(inputz, buffer);
   			endLineErase(inputz);
   			
   			passwordValid = checkPasswordValid(inputz);
   			while(!passwordValid){
   				send(new_socket , "Password must contain 6 letter, number, capital and lowercase letter" , strlen("Password must contain 6 letter, number, capital and lowercase letter") , 0 );
   				valread = read( new_socket , buffer, 1024);
   				strcpy(inputz, buffer);
   				endLineErase(inputz);
   				passwordValid = checkPasswordValid(inputz);
   			}
   			writeFilezPassword(inputz);
   			
   			send(new_socket , "Successfully Registered\nCommand: ..." , strlen("Successfully Registered\nCommand: ...") , 0 );
   		}
   		else if (strcmp(buffer, "login\n") == 0 && !logedIn){
   			//
   			//
   			//FOR LOGIN USERNAME INPUT
   			send(new_socket , "Enter your login username : " , strlen("Enter your login username : ") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(inputz, buffer);
   			endLineErase(inputz);
   			
   			usernameExist = checkStringFile(inputz);
   			while(!usernameExist){
   				send(new_socket , "Username not exist" , strlen("Username not exist") , 0 );
   				valread = read( new_socket , buffer, 1024);
   				strcpy(inputz, buffer);
   				endLineErase(inputz);
   				usernameExist = checkStringFile(inputz);
   			}

   			strcpy(username, inputz);
   			
   			//FOR LOGIN PASSWORD INPUT
   			send(new_socket , "Enter your login password : " , strlen("Enter your login password : ") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(inputz, buffer);
   			endLineErase(inputz);
   			
   			passwordValid = checkPasswordFile(inputz, username);
   			while(!passwordValid){
   				send(new_socket , "Password not match" , strlen("Password not match") , 0 );
   				valread = read( new_socket , buffer, 1024);
   				strcpy(inputz, buffer);
   				endLineErase(inputz);
   				passwordValid = checkPasswordFile(inputz, username);
   			}
   			logedIn = true;
   			send(new_socket , "Successfully Login\nCommand: 1. add    2. see    3. download <judul-problem>    4. submit <judul-problem> <path-file-output.txt>" , strlen("Successfully Login\nCommand: 1. add    2. see    3. download <judul-problem>    4. submit <judul-problem> <path-file-output.txt>") , 0 );
   		}
   		else if(!logedIn){
   			send(new_socket , "Please choose register or login" , strlen("Please choose register or login") , 0 );
   		}
   		else if(logedIn && strcmp(buffer, "add\n") == 0){
   			char judul[100] = "";
   			char descriptionPath[100] = "";
   			char inputPath[100] = "";
   			char outputPath[100] = "";
   			send(new_socket , "Judul problem:" , strlen("Judul problem:") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(judul, buffer);
   			endLineErase(judul);
   			
   			send(new_socket , "Filepath description.txt:" , strlen("Filepath description.txt:") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(descriptionPath, buffer);
   			endLineErase(descriptionPath);
   			
   			send(new_socket , "Filepath input.txt:" , strlen("Filepath input.txt:") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(inputPath, buffer);
   			endLineErase(inputPath);
   			
   			send(new_socket , "Filepath output.txt:" , strlen("Filepath output.txt:") , 0 );
   			valread = read( new_socket , buffer, 1024);
   			strcpy(outputPath, buffer);
   			endLineErase(outputPath);
   			
   			writeDatabase(judul, username);
   			int check = mkdir(judul, 0777);
   			char pathJudul[100] = "";
   			
   			//copy descriptionfile
   			char pathDescriptionClient[100] = "";
   			strcpy(pathDescriptionClient, "../client/");
   			strcat(pathDescriptionClient, descriptionPath);
   			
   			strcpy(pathJudul, judul);
   			strcat(pathJudul, "/descriptions.txt");
   			copyFile(pathDescriptionClient, pathJudul);
   			
   			//copy inputFile
   			char pathInputClient[100] = "";
   			strcpy(pathInputClient, "../client/");
   			strcat(pathInputClient, inputPath);
   			
   			strcpy(pathJudul, judul);
   			strcat(pathJudul, "/inputs.txt");
   			copyFile(pathInputClient, pathJudul);
   			
   			//copy outputFile
   			char pathOutputClient[100] = "";
   			strcpy(pathOutputClient, "../client/");
   			strcat(pathOutputClient, outputPath);
   			
   			strcpy(pathJudul, judul);
   			strcat(pathJudul, "/outputs.txt");
   			copyFile(pathOutputClient, pathJudul);
   			
   			send(new_socket , "Add complete" , strlen("Add complete") , 0 );
   			
   		}
   		else if(logedIn && strcmp(buffer, "see\n") == 0){
   			seeProblem();
   			send(new_socket , "See complete" , strlen("See complete") , 0 );
   		}
   		else if(logedIn && strstr(buffer, "download")){
   			char judul[100] = "";
   			judulDownload(buffer, judul);
   			char pathClient[100] = "";
   			char clientDescription[100] = "";
   			char clientInput[100] = "";
   			
   			//make directory in client
   			strcpy(pathClient, "../client/");
   			strcat(pathClient, judul);
   			strcat(pathClient, "/");
   			int checkz = mkdir(pathClient, 0777);
   			
   			//path in client
   			strcpy(clientDescription, pathClient);
   			strcat(clientDescription, "descriptions.txt");
   			strcpy(clientInput, pathClient);
   			strcat(clientInput, "inputs.txt");
   			
   			//path in server
   			char pathDescription[100] = "";
   			char pathInput[100] = "";
   			strcpy(pathDescription, judul);
   			strcat(pathDescription, "/description.txt");
   			strcpy(pathInput, judul);
   			strcat(pathInput, "/input.txt");
   			
   			//copy files
   			copyFile(pathDescription, clientDescription);
   			copyFile(pathInput, clientInput);
   			
   			send(new_socket , "Download complete" , strlen("Download complete") , 0 );
   		}
   		else if(logedIn && strstr(buffer, "submit")){
   			char judul[100] = "";
   			char output[100] = "";
   			judulPathSubmit(buffer, judul, output);
   			
   			char outputPath[100] = "";
   			char outputClientPath[100] = "";
   			strcpy(outputPath, judul);
   			strcat(outputPath, "/output.txt");
   			
   			strcpy(outputClientPath, "../client/");
   			strcat(outputClientPath, output);
   			
   			bool ac = eval(outputPath, outputClientPath);
   			if(ac){
   				send(new_socket , "AC" , strlen("AC") , 0 );
   			}
   			else{
   				send(new_socket , "WA" , strlen("WA") , 0 );
   			}
   			
   		}
    	printf("Message sent\n");
    }
    
    return 0;
}
